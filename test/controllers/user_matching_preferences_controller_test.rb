require 'test_helper'

class UserMatchingPreferencesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:christian)
    @user_matching_preference = UserMatchingPreference.create!(
      user_id: @user.id,
      dm: 0,
      player: 1,
      play_style: 2,
      rule_style: 8,
      combat_level: 5,
      party_size: 4,
      campaign_duration: 1,
      campaign_frequency: 1,
      serious_level: 8,
      pvp: 1 
    )
    @user_matching_preference2 = UserMatchingPreference.create!(
      user_id: 100,
      dm: 0,
      player: 1,
      play_style: 2,
      rule_style: 8,
      combat_level: 5,
      party_size: 4,
      campaign_duration: 1,
      campaign_frequency: 1,
      serious_level: 8,
      pvp: 1 
    )
  end

  #dont want index for these
  # test "should get index" do
  #   get user_matching_preferences_url
  #   assert_response :success
  # end

  test "should get new if logged in" do
    log_in_as(@user)
    get new_user_matching_preference_url
    assert_response :success
  end

  test "should redirect new if not logged in" do
    get new_user_matching_preference_url
    assert_redirected_to login_url
  end

  test "should create user_matching_preference" do
    log_in_as(@user)
    assert_difference('UserMatchingPreference.count') do
      post user_matching_preferences_url, params: { user_matching_preference: { 
        user_id: @user.id,
        dm: 0,
        player: 1,
        play_style: 2,
        rule_style: 8,
        combat_level: 5,
        party_size: 4,
        campaign_duration: 1,
        campaign_frequency: 1,
        serious_level: 8,
        pvp: 1  
      } }
    end

    assert_redirected_to user_matching_preference_url(UserMatchingPreference.last)
  end

  test "should show user_matching_preference" do
    log_in_as(@user)
    get user_matching_preference_url(@user_matching_preference)
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@user)
    get edit_user_matching_preference_url(@user_matching_preference)
    assert_response :success
  end

  test "should update user_matching_preference" do
    log_in_as(@user)
    patch user_matching_preference_url(@user_matching_preference), params: { 
                                                                      user_matching_preference: { 
                                                                        dm: true, user: false } }
    assert_redirected_to @user_matching_preference
  end

  test "should destroy user_matching_preference" do
    log_in_as(@user)
    assert_difference('UserMatchingPreference.count', -1) do
      delete user_matching_preference_url(@user_matching_preference)
    end

    assert_redirected_to user_matching_preferences_url
  end
end
