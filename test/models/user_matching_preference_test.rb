require 'test_helper'

class UserMatchingPreferenceTest < ActiveSupport::TestCase
  def setup
    @user = User.create!(name: "Example ueser", email: "example@user.com",
                     password: "foobar", password_confirmation: "foobar")
    @user_matching_preference = UserMatchingPreference.create!(
      user_id: @user.id,
      dm: 0,
      player: 1,
      play_style: 2,
      rule_style: 8,
      combat_level: 5,
      party_size: 4,
      campaign_duration: 1,
      campaign_frequency: 1,
      serious_level: 8,
      pvp: 1 
    )
  end
  
  test "should belong to a user" do
    assert_equal @user_matching_preference.user, @user
  end

  test "all attrubutes should be present" do
    @user_matching_preference.update_attribute(:dm, nil)
    assert !@user_matching_preference.valid?
  end
end
