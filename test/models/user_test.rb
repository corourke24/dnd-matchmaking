require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.create!(name: "Example user", email: "example@user.com",
                     password: "foobar", password_confirmation: "foobar")
    @user2 = User.create!(name: "Example user2", email: "example@user2.com",
                     password: "foobar2", password_confirmation: "foobar2")
    @user_matching_preference = UserMatchingPreference.create!(
      user_id: @user.id,
      dm: 0,
      player: 1,
      play_style: 2,
      rule_style: 8,
      combat_level: 5,
      party_size: 4,
      campaign_duration: 1,
      campaign_frequency: 1,
      serious_level: 8,
      pvp: 1 
    )
  end
  
  test "user should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end
  
  test "name shouldn't be too long" do 
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "emails shouldn't be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept valid emails" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |email|
      @user.email = email
      assert @user.valid?
    end
  end
  
  test "email validation should reject invalid emails" do 
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |email|
      @user.email = email
      assert_not @user.valid?
    end
  end
  
  test "email addesses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "email address should be saved in lowercase" do 
    mix_email = "EXampLE@EXAmplE.coM"
    @user.email = mix_email
    @user.save
    assert_equal mix_email.downcase, @user.reload.email
  end
  
  test "password should be present" do
    @user.password = @user.password_confirmation = ""
    assert_not @user.valid?
  end
  
  test "password should be at least 6 characters" do 
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end  

  test "should have one user_matching_preference" do
    assert_equal @user.user_matching_preference, @user_matching_preference
  end

  test "should return false if user mapping preference" do
    assert_equal @user.has_user_matching_preference?, true
  end

  test "should return false if no user mapping preference" do
    assert_equal @user2.has_user_matching_preference?, false
  end
end
