class AddAttributesToUserMatchingPreferences < ActiveRecord::Migration[5.1]
  def change
      add_column :user_matching_preferences, :user_id,              :integer
      add_column :user_matching_preferences, :dm,                   :boolean
      add_column :user_matching_preferences, :player,               :boolean
      add_column :user_matching_preferences, :play_style,           :integer
      add_column :user_matching_preferences, :rule_style,           :integer
      add_column :user_matching_preferences, :combat_level,         :integer
      add_column :user_matching_preferences, :party_size,           :integer
      add_column :user_matching_preferences, :campaign_duration,    :integer
      add_column :user_matching_preferences, :campaign_frequency,   :integer
      add_column :user_matching_preferences, :serious_level,        :integer
      add_column :user_matching_preferences, :pvp,                  :integer
  end
end
