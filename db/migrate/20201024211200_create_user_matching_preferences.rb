class CreateUserMatchingPreferences < ActiveRecord::Migration[5.1]
  def change
    create_table :user_matching_preferences do |t|

      t.timestamps
    end
  end
end
