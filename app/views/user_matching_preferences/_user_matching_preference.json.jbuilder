json.extract! user_matching_preference, :id, :created_at, :updated_at
json.url user_matching_preference_url(user_matching_preference, format: :json)
