class UserMatchingPreference < ApplicationRecord
    validates :user_id, presence: true
    validates :dm, inclusion: { in: [ true, false ] }
    validates :player, inclusion: { in: [ true, false ] }
    validates :play_style, presence: true
    validates :rule_style, presence: true
    validates :combat_level, presence: true
    validates :party_size, presence: true
    validates :campaign_duration, presence: true
    validates :campaign_frequency, presence: true
    validates :serious_level, presence: true
    validates :pvp, presence: true

    belongs_to :user,
        foreign_key: :user_id
end
