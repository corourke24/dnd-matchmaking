class UserMatchingPreferencesController < ApplicationController
  before_action :set_user_matching_preference, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user?, only: [:new, :show, :edit, :update, :destroy]
  before_action :correct_user_preference, only: [:edit, :update]

  def logged_in_user?
    unless logged_in?
      store_location
      flash[:danger] = "Please log in to continue"
      redirect_to login_url
    end
  end

  #dont want index of these
  # GET /user_matching_preferences
  # GET /user_matching_preferences.json
  # def index
  #   @user_matching_preferences = UserMatchingPreference.all
  # end

  # GET /user_matching_preferences/1
  # GET /user_matching_preferences/1.json
  def show
  end

  # GET /user_matching_preferences/new
  def new
    @user = current_user
    if @user.has_user_matching_preference?
      redirect_to @user.user_matching_preference
    else
      @user_matching_preference = UserMatchingPreference.new
    end
  end

  # GET /user_matching_preferences/1/edit
  def edit
    @user_matching_preference = UserMatchingPreference.find(params[:id])
  end

  # POST /user_matching_preferences
  # POST /user_matching_preferences.json
  def create
    @user_matching_preference = UserMatchingPreference.new(user_matching_preference_params)

    respond_to do |format|
      if @user_matching_preference.save
        format.html { redirect_to @user_matching_preference, notice: 'User matching preference was successfully created.' }
        format.json { render :show, status: :created, location: @user_matching_preference }
      else
        format.html { render :new }
        format.json { render json: @user_matching_preference.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_matching_preferences/1
  # PATCH/PUT /user_matching_preferences/1.json
  def update
    @user_matching_preference = UserMatchingPreference.find(params[:id])
    if @user_matching_preference.update_attributes(user_matching_preference_params)
      flash[:success] = "Preferences Updated!"
      redirect_to @user_matching_preference
    else
      render 'edit'
    end
  end

  # DELETE /user_matching_preferences/1
  # DELETE /user_matching_preferences/1.json
  def destroy
    @user_matching_preference.destroy
    respond_to do |format|
      format.html { redirect_to user_matching_preferences_url, notice: 'User matching preference was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_matching_preference
      @user_matching_preference = UserMatchingPreference.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_matching_preference_params
      #params.fetch(:user_matching_preference, {})     
      params.require(:user_matching_preference).permit(:user_id, :dm, :player, :play_style, :rule_style, :combat_level, :party_size, :campaign_duration, :campaign_frequency, :serious_level, :pvp)
    end

    def correct_user_preference
      @user = current_user
      @user_matching_preference = UserMatchingPreference.find(params[:id])
      redirect_to(root_url) unless @user.user_matching_preference == @user_matching_preference
    end
end
